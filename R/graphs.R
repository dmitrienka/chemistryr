
make_connectivity <- function(xyz, bonds = guess_bonds(xyz, ...), ...){
    g <- igraph::make_graph(bonds %>% as.matrix %>% t,
                            n = nrow(xyz),
                            directed = FALSE)
    igraph::V(g)$color  <- dplyr::left_join(xyz, cpk_colours, by = "Element")$N.charge
    g
}



multiplicity <- function(xyz,
                         connectivity = make_connectivity(xyz),
                         treshold = 0.001,
                         value = "mutliplicity",
                         max_automorphisms = 10000){
  as <- igraph::automorphisms(connectivity)$group_size %>% as.numeric()
  if (as > max_automorphisms){
    msg <- paste("Automorphisms group size is ", as, ".",
    "It exceeds max_automorphisms parameter which is ", max_automorphisms, ".",
    "The calculation will probably take too long.
Try to delete H or other atoms to make graph smaller
without affecting the symmetry. Alternatively, increase
max_automorphisms or stop using graph-based methods.")
    warning(strwrap(msg, prefix = " ", initial = "" ))
    return(NA)
  }
  isms <- igraph::isomorphisms(connectivity, connectivity) %>%
    lapply(as.numeric)

  rmsds <- sapply(isms, function(x){overlay(xyz[x,], xyz)})

  if (value == "mutliplicity")
    return( sum(rmsds <= treshold))
  if (value == "rmsds")
    return(rmsds)
  stop("Value should be 'mutiplicity' or 'rmsds'")
}


best_overlay <- function(xyz, target,
                         xyz.connectivity = make_connectivity(xyz),
                         target.connectivity = make_connectivity(target),
                         value = "rmsd",
                         allow.improper = FALSE){
  as <- igraph::automorphisms(xyz.connectivity)$group_size %>% as.numeric()
  if (as > max_automorphisms){
    message("Automorphisms group size is ", as, ".")
    message("It exceeds max_automorphisms parameter which is ", max_automorphisms, ".")
    message("The calculation will probably take too long.")
    message("Try to delete H or other atoms to make graph smaller without affecting the symmetry.")
    message("Alternatively, increase max_automorphisms or stop using graph-based methods.")
    stop("Too complex/symmetric molecular graph.")
  }

  isms <- igraph::isomorphisms(xyz.connectivity, target.connectivity) %>%
        lapply(as.numeric)

  rmsds <- sapply(isms, function(x){overlay(xyz[x,], target,
                                              value = "rmsd",
                                              allow.improper)})

    best <- isms[[which.min(rmsds)]]

    result <- overlay(xyz[best,], target, value, allow.improper)

    if (value == "move")
        result$map <- best
    result
    
}
