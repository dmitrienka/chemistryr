---
title: "symmetry_number"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{symmetry_number}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```
  Загружаем библиотеки: собственно `chemistryr`, а еще `dplyr`, чтобы
  пользоваться `%>%` и `filter`, и `purrr`, в котором есть `map_*`.
  
  `Sys.setenv(RGL_USE_NULL = TRUE)` и  `rgl::setupKnitr()`
  помогают встраивать WebGL в HTML и в обычной жизни не нужны. 

```{r setup}
Sys.setenv(RGL_USE_NULL = TRUE) ## don't show rgl x11 windows 
library(chemistryr)
library(dplyr)
library(purrr)
rgl::setupKnitr()
```

# Читаем файлы 

Библиотека умеет читать `xyz` файлы с помощью функции `read_xyz()`. Три примера
(которые мне прислал Вадим)  включены в библиотеку; путь к ним после установки
доступен через `system.file()`. Они лежат в папке `extdata` и называются
`Fe12-Cu20.xyz`, `Fe12-Cu12.xyz` и `Fe12-Cu11_28.xyz`:

```{r}
cu12.path  <- system.file("extdata", "Fe12-Cu12.xyz", package="chemistryr")
cu12 <- read_xyz(cu12.path)
cu12
```

```{r}
cu20 <- system.file("extdata", "Fe12-Cu20.xyz", package="chemistryr") %>% read_xyz()
cu11 <- system.file("extdata", "Fe12-Cu11_28.xyz", package="chemistryr") %>% read_xyz()
```

Можно нарисовать что-нибудь для примера:
  
```{r}
plot(cu11)
rgl::rglwidget(width=700, height=600) # генерирует html виджет, обычно не нужен
```

# Порядок группы симметрии

Порядок группы симметрии вычисляется функцией `multiplicity()`. Она работает
так: сначала вычисляет все симметрии (автоморфизмы) графа связности, без учета
пространственного расположения атомов, потом накладывает их друг на друга, чтобы
проверить, какие из них является так же симметриями собственно молекулы.
Молекула не может иметь более высокую симметрию, чем её граф связности, а
наоборот --- пожалуйста. 

Группы автоморфизмов молекулярных графов иногда бывают очень
большими --- например, каждая CH<sub>2</sub> группа увеличивает число элементов вдвое 
(потому что поменять местами эти водороды это автоморфизм). Поэтому имеет смысл
убрать из молекулы всё, что не влияет на симметрию. Для большинства молекул
достаточно удалить водороды. Функция `multiplicity()` имеет параметр
`max_automorphisms`, если число автоморфизмов графа превышает его значение,
функция возвращает `NA` (и warning).

На втором этапе вычисляются RMSD наложений молекул. Часто встречается неточная
симметрия: молекула и её преобразованная копия накладываются с низким, но не
нулевым RMSD. Функция `multiplicity()` позволяет выбрать пороговое значение с
помощью параметра `treshold`. Также можно посмотреть на все получившиеся RMSD,
вызвав `multiplicity()` с параметром `value = "rmsds"`. 

  
```{r}
multiplicity(cu12)

cu12 %>% filter(Element != "H") %>% multiplicity()

```

Даже после удаления водородов размер группы симметрий графа превышает
10<sup>13</sup>. К счастью, в случае медных шаров удаление всех углеродов тоже
не меняет симметрию молекулы. Так можно, наконец, получить искомое значение.

```{r}
cu12.trimmed  <- cu12 %>% filter(! Element %in% c("C", "H"))
plot(cu12.trimmed)
rgl::rglwidget(width=700, height=600)
```

```{r}
multiplicity(cu12.trimmed)

```

Применим тот же подход к оставшимся молекулам:

```{r}
cu20 %>% filter(! Element %in% c("C", "H")) %>% multiplicity()

cu11 %>% filter(! Element %in% c("C", "H")) %>% multiplicity()
```

Упс, полученные значения меньше ожидаемых. Посмотрим на все рассчитанные RMSD:

```{r}
cu11 %>% filter(! Element %in% c("C", "H")) %>% multiplicity(value="rmsds")
cu20 %>% filter(! Element %in% c("C", "H")) %>% multiplicity(value="rmsds")
```

Видно много несовершенных наложений с RMSD 0.003 у `cu11` и  0.013 у `cu20`.
Выставив `treshold = 0.1` можно получить ожидаемые значения:

```{r}
cu20 %>% filter(! Element %in% c("C", "H")) %>% multiplicity(treshold=0.1)
cu11 %>% filter(! Element %in% c("C", "H")) %>% multiplicity(treshold=0.1)
```

Вот простая программа, вычисляющая по такой схеме порядок группы симметрии для
всех  `xyz` файлов в папке:


```{r}
my.dir <- system.file("extdata", package="chemistryr") # путь к папке
files <- dir(my.dir, pattern = "[.]xyz$", full.names=T)

sigmas <- map_int(files,
                  ~ .x %>% read_xyz %>%
                    filter(! Element %in% c("C", "H")) %>%
                    multiplicity(treshold=0.1))

tibble(File = basename(files), Sigma = sigmas)

```
